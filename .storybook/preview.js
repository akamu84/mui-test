import {ThemeProvider} from "@emotion/react";
import {theme} from "../src/theme";
import {CssBaseline} from "@mui/material";

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  layout: "centered",
  controls: {
    expanded: true,
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}

export const decorators = [
  (Story) => (
      <ThemeProvider theme={theme}>
        <CssBaseline/>
        <Story/>
      </ThemeProvider>
  ),
]
