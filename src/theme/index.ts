import {createTheme, Theme} from "@mui/material";
import {typography} from "./foundations/typography";
import {palette} from "./foundations/palette";
import {components} from "./components";
import {breakpoints} from "./foundations/breakpoints";


export const theme: Theme = createTheme({
    breakpoints,
    typography,
    palette,
    components
});
