import {PaletteOptions} from "@mui/material";

export const palette: PaletteOptions = {
    common: {
      black: '#000',
      white: '#fff'
    },
    primary: {
        light: '#e73662',
        main: '#d11947',
        contrastText: '#fff'
    },
    secondary: {
        light: '#2881e8',
        main: '#135cb0',
        contrastText: '#fff'
    },
    error: {
        main: '#e03400'
    },
    success: {
        main: '#008a1c'
    },
    info: {
        main: '#0d4d55'
    },
    warning: {
        main: '#4d4d4d'
    },
    text: {
        primary: '#1a1a1a',
    }
};
