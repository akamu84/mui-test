import {TypographyOptions} from "@mui/material/styles/createTypography";

const fonts = ['"SJ Sans"', '"Open Sans"', '"Helvetica Neue"', "Helvetica", "Arial", "sans-serif"].join(',');

export const typography: TypographyOptions = {
    fontSize: 16,
    fontFamily: fonts,
    fontWeightMedium: 600,
    fontWeightBold: 800,
    button: {
        fontWeight:  600,
        fontFamily: fonts,
        textTransform: 'capitalize'
    },
    h1: {
        fontSize: '2rem',
        fontFamily: fonts
    },
    h2: {
        fontSize: '1.625rem',
        fontFamily: fonts
    },
    h3: {
        fontSize: '1.313rem',
        fontFamily: fonts
    },
    h4: {
        fontSize: '1.125rem',
        fontFamily: fonts
    },
    h5: {
        fontSize: '0.875rem',
        fontFamily: fonts
    },
    h6: {
        fontSize: '.75rem',
        fontFamily: fonts
    },
    subtitle1: {
        fontSize: '.75rem',
        fontFamily: fonts
    },
    subtitle2: {
        fontSize: '.75rem',
        fontFamily: fonts
    },
    body1: {
        fontSize: '1rem',
        fontFamily: fonts
    },
    body2: {
        fontSize: '0.875rem',
        fontFamily: fonts
    },
    caption: {
        fontFamily: fonts
    },
    overline: {
        fontFamily: fonts
    }
}
