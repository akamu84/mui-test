export const MuiCssBaseline = {
    styleOverrides: `
        @font-face {
          font-family: 'SJ Sans';
          font-style: normal;
          font-display: swap;
          font-weight: 300;
          src: url(https://www.stjude.org/etc/clientlibs/stjude/shared/fonts/sj-sans/sjs-light.woff2) format('woff2'),url(https://www.stjude.org/etc/clientlibs/stjude/shared/fonts/sj-sans/sjs-light.woff) format("woff");
        }
        @font-face {
          font-family: 'SJ Sans';
          font-style: normal;
          font-display: swap;
          font-weight: 400;
          src: url(https://www.stjude.org/etc/clientlibs/stjude/shared/fonts/sj-sans/sjs-regular.woff2) format('woff2'),url(https://www.stjude.org/etc/clientlibs/stjude/shared/fonts/sj-sans/sjs-regular.woff) format("woff");
        }
        @font-face {
          font-family: 'SJ Sans';
          font-style: normal;
          font-display: swap;
          font-weight: 600;
          src: url(https://www.stjude.org/etc/clientlibs/stjude/shared/fonts/sj-sans/sjs-medium.woff2) format('woff2'),url(https://www.stjude.org/etc/clientlibs/stjude/shared/fonts/sj-sans/sjs-medium.woff) format("woff");
        }
        @font-face {
          font-family: 'SJ Sans';
          font-style: normal;
          font-display: swap;
          font-weight: 700;
          src: url(https://www.stjude.org/etc/clientlibs/stjude/shared/fonts/sj-sans/sjs-book.woff2) format('woff2'),url(https://www.stjude.org/etc/clientlibs/stjude/shared/fonts/sj-sans/sjs-book.woff) format("woff");
        }
        @font-face {
          font-family: 'SJ Sans';
          font-style: normal;
          font-display: swap;
          font-weight: 800;
          src: url(https://www.stjude.org/etc/clientlibs/stjude/shared/fonts/sj-sans/sjs-bold.woff2) format('woff2'),url(https://www.stjude.org/etc/clientlibs/stjude/shared/fonts/sj-sans/sjs-bold.woff) format("woff");
        }
      `,
}
