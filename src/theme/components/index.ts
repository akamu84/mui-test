import {MuiCssBaseline} from "./MuiCssBaseline";
import {Components} from "@mui/material";
import {MuiButton} from "./MuiButton";
import {MuiTextField} from "./MuiTextField";

export const components: Components = {
    MuiCssBaseline,
    MuiButton,
    MuiTextField,
}
