import {Components, createTheme, darken} from "@mui/material";
import {palette} from "../foundations/palette";

const { palette: {primary, secondary} } = createTheme({
    palette
});

export const MuiButton: Components['MuiButton'] = {
    variants: [
        {
            props: {variant: 'dashed'},
            style: {
                textTransform: 'none',
                border: `2px dashed ${primary.light}`,
            },
        },
        {
            props: {variant: 'dashed', color: 'secondary'},
            style: {
                border: `4px dashed ${primary.light}`,
            },
        },
    ],
    styleOverrides: {
        root: {
            "&.MuiButton-dashedPrimary": {
                backgroundColor: "#000",
                color: "#fff"
            },
        },
        containedPrimary: {
            '&:hover': {
                backgroundColor: primary.light
            },
            border: '1px',
            borderBottom: `solid 2px ${darken(primary.main, 0.22)}`
        },
        containedSecondary: {
            '&:hover': {
                backgroundColor: secondary.light
            },
            borderBottom: `solid 2px ${darken(secondary.main, 0.22)}`
        },
    },
}
