import {createTheme} from "@mui/material";
import {palette} from "../foundations/palette";

const {palette: {text}} = createTheme({
    palette
});

export const MuiTextField = {
    styleOverrides: {
        root: {
            label: {
                position: 'initial',
                textAlign: 'left',
                transform: 'none',
                color: text.primary,
            }
        }
    }
}
