import React from 'react';
import { Story, Meta } from '@storybook/react';

import {ButtonProps} from '@mui/material';
import {ButtonWrapper} from "./wrappers/ButtonWrapper";

export default {
  title: 'Button',
  component: ButtonWrapper,
  argTypes: {} as ButtonProps,
} as Meta;

const Template: Story<ButtonProps> = (args) => <ButtonWrapper {...args}>test</ButtonWrapper>;

export const Primary = Template.bind({});
Primary.args = {
  variant: 'dashed'
}
