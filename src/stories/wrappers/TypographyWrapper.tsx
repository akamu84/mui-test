import React, {FC} from "react";
import {Typography, TypographyProps} from "@mui/material";

export const TypographyWrapper: FC<TypographyProps> = (args) => <Typography {...args} />
