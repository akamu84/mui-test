import {TextField, TextFieldProps} from "@mui/material";
import React, {FC} from "react";

export const TextFieldWrapper: FC<TextFieldProps> = (args) => <TextField {...args} />
