import React, {FC} from "react";
import {Button, ButtonProps} from "@mui/material";

export const ButtonWrapper: FC<ButtonProps> = (args) => <Button {...args} />
