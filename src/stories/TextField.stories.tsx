import React from 'react';
import { Story, Meta } from '@storybook/react';

import {TextFieldProps} from '@mui/material';
import {TextFieldWrapper} from "./wrappers/TextFieldWrapper";

export default {
  title: 'TextField',
  component: TextFieldWrapper,
  argTypes: {} as TextFieldProps,
} as Meta;

const Template: Story<TextFieldProps> = (args) => <TextFieldWrapper {...args}>test</TextFieldWrapper>;

export const Primary = Template.bind({});
Primary.args = {
  label: 'Test Field',
  variant: 'outlined',
  color: 'secondary',
  required: true,
  InputLabelProps: {
    shrink: true
  }
}
