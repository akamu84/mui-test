import React from 'react';
import { Story, Meta } from '@storybook/react';

import {TypographyProps} from '@mui/material';
import {TypographyWrapper} from "./wrappers/TypographyWrapper";

export default {
  title: 'Typography',
  component: TypographyWrapper,
  argTypes: {} as TypographyProps,
} as Meta;

const Template: Story<TypographyProps> = () => (<>
  <TypographyWrapper variant="h1" gutterBottom>
    h1. Heading
  </TypographyWrapper>
  <TypographyWrapper variant="h2" gutterBottom>
    h2. Heading
  </TypographyWrapper>
  <TypographyWrapper variant="h3" gutterBottom>
    h3. Heading
  </TypographyWrapper>
  <TypographyWrapper variant="h4" gutterBottom>
    h4. Heading
  </TypographyWrapper>
  <TypographyWrapper variant="h5" gutterBottom>
    h5. Heading
  </TypographyWrapper>
  <TypographyWrapper variant="h6" gutterBottom>
    h6. Heading
  </TypographyWrapper>
  <TypographyWrapper variant="subtitle1" gutterBottom>
    subtitle1. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos
    blanditiis tenetur
  </TypographyWrapper>
  <TypographyWrapper variant="subtitle2" gutterBottom>
    subtitle2. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos
    blanditiis tenetur
  </TypographyWrapper>
  <TypographyWrapper variant="body1" gutterBottom>
    body1. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos
    blanditiis tenetur unde suscipit, quam beatae rerum inventore consectetur,
    neque doloribus, cupiditate numquam dignissimos laborum fugiat deleniti? Eum
    quasi quidem quibusdam.
  </TypographyWrapper>
  <TypographyWrapper variant="body2" gutterBottom>
    body2. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos
    blanditiis tenetur unde suscipit, quam beatae rerum inventore consectetur,
    neque doloribus, cupiditate numquam dignissimos laborum fugiat deleniti? Eum
    quasi quidem quibusdam.
  </TypographyWrapper>
  <TypographyWrapper variant="button" display="block" gutterBottom>
    button text
  </TypographyWrapper>
  <TypographyWrapper variant="caption" display="block" gutterBottom>
    caption text
  </TypographyWrapper>
  <TypographyWrapper variant="overline" display="block" gutterBottom>
    overline text
  </TypographyWrapper>
</>);

export const Primary = Template.bind({});
